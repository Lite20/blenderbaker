import bpy
import os

scene = bpy.context.scene
for obj in scene.objects:
    obj.select = False

for obj in scene.objects:
    if obj.type != 'MESH':
        continue

    scene.objects.active = obj
    obj.select = True

    bpy.ops.object.bake(type='COMBINED')
    obj.select = False

# save all baked images
for image in bpy.data.images:
    if image.is_dirty:
        print(image)
        # foo.png -> bake_foo.png
        image.filepath_raw = bpy.path.abspath("//") + "baked_" + image.name + ".png"
        image.save()
