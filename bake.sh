# NOTE: Script requires human interaction (at the end)
# bake textures
./blender-2.79-linux-glibc219-x86_64/blender --background build/blend.blend --python build/bake_all.py --threads 16

# move them to out directory
rm -rf build/out/
mkdir build/out/
mv build/baked_* build/out/

# package textures & clean up
cd build
tar -zcvf out.tar.gz out/
rm -rf out/
cd ..

# upload results to github
# NOTE: push requires password (human interaction)
git config --global user.email "hamster.team1@gmail.com"
git add . --all
git commit -am "completed render"
git push
